SET @username = TRIM(TRAILING '.*' FROM 'playground');

SET @query1 = CONCAT('
    GRANT ALL ON *.* TO "',@username,'"@"localhost" IDENTIFIED BY "password" WITH GRANT OPTION'
);
PREPARE stmt FROM @query1; EXECUTE stmt; DEALLOCATE PREPARE stmt;