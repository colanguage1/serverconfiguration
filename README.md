# Bootstrapping files for a server config #

Configuration files to set up a basic server on a Linux (APT-based) machine.

## Use:
* `sudo make`: will setup the server from scratch, installing Nginx, PHP, MariaDB, and other tools.
* `sudo make <target>`: will specify a target. These are as follow:
    * `init`: updates the APT databases and upgrades the server packages.
    * `nginx`: Nginx web server
    * `mariadb`: MariaDB SQL Database
    * `php5`: PHP 5 (for Drupal 7)
    * `php7`: PHP 7 (for Drupal 8+)