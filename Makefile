ifndef SITE_NAME
SITE_NAME := playground
endif
DATE := $(date +'%F_%H-%M-%S')

all :: init nginx php mariadb fail2ban papertrail

init ::
	yes | apt update
	yes | apt upgrade
	grep -rli 'playground' * --exclude=Makefile | xargs -i@ sed -i "s/playground/$(SITE_NAME)/g" @

nginx :: init
	yes | apt install nginx
	-mkdir -p /var/www/$(SITE_NAME)
	tar -czf nginx_$(DATE)tar.gz /etc/nginx
	cp -r config/nginx /etc/
	-ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf
	systemctl enable --now nginx

php :: init nginx
	yes | apt install php php-fpm php-gd php-mysql php-xml php-curl composer
	cp tests/index.php /var/www/$(SITE_NAME)/
	systemctl restart nginx

mariadb :: init
	yes | apt install mariadb-server
	cp -r config/mysql/conf.d /etc/mysql
	systemctl enable --now mariadb
	echo '\n n\n y\n' | mysql_secure_installation
	mysql -uroot < config/mysql/setup.sql

fail2ban :: init
	yes | apt install fail2ban
	cp -r config/fail2ban/ /etc/
	systemctl enable fail2ban --now

papertrail :: init
	cp config/papertrail/log_files.yml /etc/

drupal7 :: init clean-www
	composer create-project drupal-composer/drupal-project:7.x-dev /var/www/$(SITE_NAME) --no-interaction

drupal8 :: init clean-www
	composer create-project drupal/recommended-project /var/www/$(SITE_NAME) --no-interaction
	cd /var/www/$(SITE_NAME)
	composer require drush/drush
	-mkdir -p /var/www/$(SITE_NAME)/web/sites/default/files
	chmod 777 /var/www/$(SITE_NAME)/web/sites/default/files

clean-www ::
	tar -czf www_$(date)tar.gz /var/www/$(SITE_NAME)
	-rm -r  /var/www/$(SITE_NAME)/* /var/www/$(SITE_NAME)/.*
